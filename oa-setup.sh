#!/bin/bash
collection="us_south"
# Other example collections: "us_south", "us_midwest", "us_west", "us_northeast"

#filter="./global/us/" # comment out if not applicable
work_dir="./$collection"

echo "Downloading $collection file..."
wget https://data.openaddresses.io/openaddr-collected-${collection}.zip

echo "Unzipping $collection file..."
unzip openaddr-collected-${collection}.zip -d ${work_dir}/

for country in ${work_dir}/*/
do
	if [[ (-n "$filter"  && ${country} == ${filter}) || ! -n "$filter" ]]
	then
		echo "LON,LAT,NUMBER,STREET,UNIT,CITY,DISTRICT,REGION,POSTCODE,ID,HASH" > ./${collection}_load_file.csv
		awk FNR!=1 $country*/*.csv >> ./${collection}_load_file.csv 2>/dev/null
	fi
done

# Need a linux formated file? Use dos2unix
#dos2unix -f ./${collection}_load_file.csv
