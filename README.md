**bench4gis: benchmarking GIS with big open daa**

---

## Run oa-setup.sh

This is a simple bash script that will download addresses from openaddresses.io and merge them together for processing.

1. Edit the collection, if desired.
2. Edit the location, if desired.
3. Add a filter, if desired.  You can specifiy specific collections or you can filter from a larger collection (like global).

---

## Database-driven GIS

If you have PostGIS installed with the Tiger Geocoder:

1. Run the create/DDL PostgresSQL scripts.
2. Run the load OA script (or load data from oa-setup.sh as you normally would; it is just a CSV file).
3. Run the create random sample PostgreSQL script, if desired.
4. Run the batch process PostgreSQL script.  By default, we picked batches of 1000 addresses but configure this according to what load your system can handle.  Run as many of these as needed.

---

## Web-services-driven GIS

If you have a web-services-driven application like Nominatim installed:

1. Run the pip-requirements file to make sure you have the necessary Python libraries.
2. Edit the parallel-nom.py script to your desired input file.
3. Edit the parallel-nom.py script to your desired output directory.
4. Edit the parallel-nom.py script to your desired Geocoder, if needed.  We picked Nominatim, but GeoPy supports many, many geocoders.
5. Run parallel-nom.py.  It will use N-1 CPUs, where N is the number of CPUs that Python detects for your system.

