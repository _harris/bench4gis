CREATE TABLE oa.address_geocoded_test
(
    hash text,
    original_address text,
    rating integer,
    lon double precision,
    lat double precision,
    stno integer,
    street character varying,
    styp character varying,
    city character varying,
    st character varying,
    zip character varying
);
