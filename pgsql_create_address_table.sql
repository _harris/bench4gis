CREATE TABLE oa.address_test
(
    lon double precision,
    lat double precision,
    "number" text,
    street text,
    unit text,
    city text,
    district text,
    region text,
    postcode text,
    id text,
    hash text
);
