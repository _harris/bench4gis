DO $$DECLARE i int;
BEGIN
i = 0;
WHILE i < 1000
LOOP
	UPDATE oa.address_geocoded_test
	  SET  (rating, lon, lat, stno, street, styp, city, st, zip)
		= ( COALESCE(g.rating,-1), ST_X(g.geomout), ST_Y(g.geomout), (addy).address, (addy).streetname, 
		(addy).streettypeabbrev, (addy).location, (addy).stateabbrev,(addy).zip )
	FROM (SELECT hash, original_address
		FROM oa.address_geocoded_test
		WHERE rating IS NULL ORDER BY hash LIMIT 1000) As a
		LEFT JOIN LATERAL geocode(a.original_address,1) As g ON true
	WHERE a.hash = address_geocoded_test.hash;
	COMMIT;
	i = i + 1;
END LOOP;
END$$;

