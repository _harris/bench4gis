CREATE TABLE oa.address_random_test
(
    lon double precision,
    lat double precision,
    "number" text,
    street text,
    unit text,
    city text,
    district text,
    region text,
    postcode text,
    id text,
    hash text
);

CREATE TABLE oa.address_random_staging_test
(
    lon double precision,
    lat double precision,
    "number" text,
    street text,
    unit text,
    city text,
    district text,
    region text,
    postcode text,
    id text,
    hash text
);

CREATE TABLE oa.address_geocoded_test
(
    hash text,
    original_address text,
    rating integer,
    lon double precision,
    lat double precision,
    stno integer,
    street character varying,
    styp character varying,
    city character varying,
    st character varying,
    zip character varying
);

