select ag.hash, ag.rating,
		ar.lon as oa_lon, ar.lat as oa_lat,
		ag.lon as tg_lon, ag.lat as tg_lat,
		ST_Distance(geography(ST_MakePoint(ar.lon, ar.lat)),
                   geography(ST_MakePoint(ag.lon, ag.lat))) as tiger_distance
into oa.address_oa_vs_tiger
from oa.address_geocoded_test ag
	join oa.address_random_test ar on ag.hash = ar.hash;

select sum(case when rating >= 0 then 1 else 0 end)*1.0/1000000, min(tiger_distance), max(tiger_distance), avg(tiger_distance), stddev(tiger_distance),
	sum(case when tiger_distance <= 50 then 1 else 0 end)*1.0/1000000 as u10,
	sum(case when tiger_distance <= 100 then 1 else 0 end)*1.0/1000000 as u100,
	sum(case when tiger_distance <= 300 then 1 else 0 end)*1.0/1000000 as u300,
	sum(case when tiger_distance <= 600 then 1 else 0 end)*1.0/1000000 as u600,
	sum(case when tiger_distance <= 1609.34 then 1 else 0 end)*1.0/1000000 as u1609
from oa.address_oa_vs_tiger;

