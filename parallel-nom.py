from geopy.geocoders import Nominatim
import multiprocessing as mp
import csv

def geocode_worker(address):
	try:
		geolocator = Nominatim(user_agent="nominatim-test", domain="your-internal-nominatim-address/nominatim", scheme='http')
		location = geolocator.geocode(address["original_address"])
		filename = "nom_out/" + address["hash"]
		if(location.latitude):
			with open(filename, mode='w') as file:
    				file.write(address["hash"] + "," +  str(location.latitude) + "," + str(location.longitude) + "\n")
	except Exception as E:
		# do nothing
		pass

if __name__ == '__main__':
	with open('random-sample.csv') as csvfile:
		addresses = csv.DictReader(csvfile)
		pool = mp.Pool(processes = (mp.cpu_count() - 1))
		result = pool.map(geocode_worker, addresses)
	csvfile.close()
