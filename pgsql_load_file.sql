-- To use: replace filename with appropriate file name
-- Alternatively, load data using your normal methods.
-- If on linux, be sure to dos2unix files from OA
\copy oa.address_test (lon, lat, number, street, unit, city, district, region, postcode, id, hash) FROM 'us_south_load_file.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '\';
