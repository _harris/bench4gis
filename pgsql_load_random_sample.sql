
insert into oa.address_random_staging_test
select *
from oa.address_test tablesample BERNOULLI (1)
where 
(number is not null and number <> ' ')
and (street is not null and street <> ' ')
and (city is not null and city <> ' ')
and (postcode is not null and postcode <> ' ');
--and region in ('Edit', 'This', 'List')


with distinct_randoms as (
	select distinct lon, lat, number, street, unit, city, 
		district, region, postcode, id, hash
	from oa.address_random_staging_test
	limit 1000000
	-- Edit limit to needed sample size
)
insert into oa.address_random_test
select *
from distinct_randoms;

insert into oa.address_geocoded_test
select hash, number || ' ' || street || ', ' || city || ', ' 
	|| region || ' ' || postcode as original_address
from oa.address_random_test;

